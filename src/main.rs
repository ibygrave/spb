use std::collections::BinaryHeap;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::path::PathBuf;

use anyhow::{Context, Result};
use clap::Parser;
use regex::Regex;

/// List words
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Letter set
    #[arg(value_name = "LETTERS")]
    letters: String,

    /// Word list
    #[arg(
        short,
        long,
        value_name = "DICT",
        default_value = "/etc/dictionaries-common/words"
    )]
    dict: PathBuf,

    /// Minimum word length
    #[arg(short, long, default_value = "4")]
    length_min: usize,
}

trait ReadLines {
    fn read_lines(self) -> impl Iterator<Item = String>;
}

impl<R> ReadLines for R
where
    R: Read,
{
    fn read_lines(self: R) -> impl Iterator<Item = String> {
        BufReader::new(self).lines().map_while(Result::ok)
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
struct LongestWord(usize, String);

impl LongestWord {
    fn new(word: String) -> Self {
        Self(word.len(), word)
    }
}

fn main() -> Result<()> {
    let args = Args::parse();
    let must_contain = args.letters.chars().next().context("Not enough letters")?;
    let pattern = Regex::new(format!("^[{}]{{{},}}$", args.letters, args.length_min).as_str())?;

    let mut words = File::open(args.dict)?
        .read_lines()
        .filter(|word| pattern.is_match(word))
        .filter(|word| word.contains(must_contain))
        .map(LongestWord::new)
        .collect::<BinaryHeap<_>>();
    while let Some(LongestWord(_, word)) = words.pop() {
        println!("{word}");
    }
    Ok(())
}
